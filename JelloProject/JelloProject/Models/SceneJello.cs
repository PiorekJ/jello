﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.Core.JelloCore;
using JelloProject.OpenTK;
using JelloProject.Utils;
using OpenTK;
using InputManager = JelloProject.Core.InputManager;

namespace JelloProject.Models
{
    public class SceneJello : Model, IColorable
    {
        public Color Color { get; set; } = Colors.LawnGreen;
        public JelloPoint[,,] JelloPoints;
        public List<Spring> Springs;


        private readonly float FrameSide = Settings.JelloSide;
        public readonly int SidePoints = 4;

        public bool IsJelloVisible
        {
            get => _isJelloVisible;
            set
            {
                _isJelloVisible = value; 
                RaisePropertyChanged();
            }
        }

        public bool AreJelloPointsVisible
        {
            get => _areJelloPointsVisible;
            set
            {
                _areJelloPointsVisible = value;
                RaisePropertyChanged();
                foreach (JelloPoint point in JelloPoints)
                {
                    point.Model.IsVisible = value;
                }
            }
        }

        public int SelectedMeshIdx
        {
            get => _selectedMeshIdx;
            set
            {
                _selectedMeshIdx = value;
                switch (_selectedMeshIdx)
                {
                    case 0:
                        Mesh = _springMesh;
                        Shader = Shaders.BezierCubeShaderNonLit;
                        break;
                    case 1:
                        Mesh = _bezierPointMesh;
                        Shader = Shaders.BezierCubeShaderNonLit;
                        break;
                    case 2:
                        Mesh = _bezierLineMesh;
                        Shader = Shaders.BezierCubeShaderNonLit;
                        break;
                    case 3:
                        Mesh = _bezierTriangleMesh;
                        Shader = Shaders.BezierCubeShader;
                        break;
                    case 4:
                        Mesh = _modelMesh;
                        Shader = Shaders.BezierCubeShader;
                        break;
                }
                RaisePropertyChanged();
            }
        }

        private readonly Mesh<VertexPC> _springMesh;
        private readonly Mesh<VertexPNC> _bezierTriangleMesh;
        private readonly Mesh<VertexPNC> _bezierPointMesh;
        private readonly Mesh<VertexPC> _bezierLineMesh;
        private readonly Mesh<VertexPNC> _modelMesh;

        public int UParameter
        {
            get => _uParameter;
            set
            {
                _uParameter = value;
                SetMesh();
            }
        }

        public int VParameter
        {
            get => _vParameter;
            set
            {
                _vParameter = value;
                SetMesh();
            }
        }

        public int WParameter
        {
            get => _wParameter;
            set
            {
                _wParameter = value;
                SetMesh();
            }
        }


        private int _uParameter = 32;
        private int _vParameter = 32;
        private int _wParameter = 32;
        private VertexPC[] _vertices;
        private int _selectedMeshIdx;
        private bool _areJelloPointsVisible = true;
        private bool _isJelloVisible = true;

        public SceneJello(Scene currScene) : base(currScene)
        {
            GenerateJelloPoints();
            
            _bezierTriangleMesh = GenerateBezierCubeTriangleMesh();
            _springMesh = GenerateSpringMesh();
            _bezierPointMesh = GenerateBezierCubePointMesh();
            _bezierLineMesh = GenerateBezierCubeLineMesh();
            _modelMesh = GenerateModelMesh();
            Mesh = _modelMesh;
            SelectedMeshIdx = 4;
        }

        public override void SetMesh()
        {
            Mesh = _modelMesh;
        }

        public override void SetShader()
        {
            Shader = Shaders.BezierCubeShader;
        }

        private Mesh<VertexPNC> GenerateModelMesh()
        {
            return AssimpManager.LoadMesh("./OBJModels/monkeyF.obj");
        }

        private void GenerateJelloPoints()
        {
            JelloPoints = new JelloPoint[SidePoints, SidePoints, SidePoints];
            float dist = FrameSide / (SidePoints - 1);
            var offsetX = -FrameSide / 2;
            var offsetY = -FrameSide / 2;
            var offsetZ = -FrameSide / 2;
            for (int i = 0; i < SidePoints; i++)
            {
                for (int j = 0; j < SidePoints; j++)
                {
                    for (int k = 0; k < SidePoints; k++)
                    {
                        JelloPoints[k, i, j] =
                            new JelloPoint(
                                new Vector3(dist * k + offsetX, dist * i + offsetY,
                                    dist * j + offsetZ), Scene);
                    }
                }
            }
        }

        private Mesh<VertexPC> GenerateBezierCubeLineMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            List<uint> edges = new List<uint>();

            var distU = 1f / (UParameter - 1);
            var distV = 1f / (VParameter - 1);
            var distW = 1f / (WParameter - 1);

            for (int i = 0; i < WParameter; i++)
            {
                for (int j = 0; j < VParameter; j++)
                {
                    for (int k = 0; k < UParameter; k++)
                    {
                        vertices.Add(new VertexPC(k * distU, i * distW, j * distV, Color.ColorToVector3()));
                        if (k != 0)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)((k - 1) + UParameter * j + UParameter * VParameter * i));
                        }
                        if (j != 0)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * (j - 1) + UParameter * VParameter * i));
                        }
                        if (i != 0)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * (i - 1)));
                        }
                    }
                }
            }

            return new Mesh<VertexPC>(vertices, edges, MeshType.Lines, AccessType.Dynamic);
        }

        private Mesh<VertexPNC> GenerateBezierCubePointMesh()
        {
            List<VertexPNC> vertices = new List<VertexPNC>();

            var distU = 1f / (UParameter - 1);
            var distV = 1f / (VParameter - 1);
            var distW = 1f / (WParameter - 1);
            for (int i = 0; i < WParameter; i++)
            {
                for (int j = 0; j < VParameter; j++)
                {
                    for (int k = 0; k < UParameter; k++)
                    {

                        if (i == 0 || i == WParameter - 1 || j == 0 || j == VParameter - 1 || k == 0 ||
                            k == UParameter - 1)
                        {
                            vertices.Add(new VertexPNC(k * distU, i * distW, j * distV, Vector3.UnitY,
                                Color.ColorToVector3()));
                        }
                    }
                }
            }

            return new Mesh<VertexPNC>(vertices, null, MeshType.Points, AccessType.Dynamic);
        }

        private Mesh<VertexPNC> GenerateBezierCubeTriangleMesh()
        {
            List<VertexPNC> vertices = new List<VertexPNC>();
            List<uint> edges = new List<uint>();

            var distU = 1f / (UParameter - 1);
            var distV = 1f / (VParameter - 1);
            var distW = 1f / (WParameter - 1);
            for (int i = 0; i < WParameter; i++)
            {
                for (int j = 0; j < VParameter; j++)
                {
                    for (int k = 0; k < UParameter; k++)
                    {
                        var normal = Vector3.Zero;
                        
                        if(j == 0)
                            normal += -Vector3.UnitZ;
                        if(j == VParameter - 1)
                            normal += Vector3.UnitZ;
                        if (i == 0)
                            normal += -Vector3.UnitY;
                        if (i == WParameter - 1)
                            normal += Vector3.UnitY;
                        if (k == 0)
                            normal += -Vector3.UnitX;
                        if (k == UParameter - 1)
                            normal += Vector3.UnitX;

                        var pos = new Vector3(k * distU, i * distW, j * distV);
                        
                        vertices.Add(new VertexPNC(pos, normal.Normalized(),
                            Color.ColorToVector3()));

                        if (j != 0 && k < UParameter - 1)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * (j - 1) + UParameter * VParameter * i));
                            edges.Add((uint)(k + 1 + UParameter * (j - 1) + UParameter * VParameter * i));
                        }

                        if (j != 0 && k != 0)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k - 1 + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * (j - 1) + UParameter * VParameter * i));
                        }

                        if (i != 0 && k != 0)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * (i - 1)));
                            edges.Add((uint)(k - 1 + UParameter * j + UParameter * VParameter * (i - 1)));

                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k - 1 + UParameter * j + UParameter * VParameter * (i - 1)));
                            edges.Add((uint)(k - 1 + UParameter * j + UParameter * VParameter * i));
                        }

                        if (i != 0 && j != 0)
                        {
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * (j - 1) + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * (j - 1) + UParameter * VParameter * (i - 1)));

                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * i));
                            edges.Add((uint)(k + UParameter * (j - 1) + UParameter * VParameter * (i - 1)));
                            edges.Add((uint)(k + UParameter * j + UParameter * VParameter * (i - 1)));
                        }

                    }
                }
            }

            return new Mesh<VertexPNC>(vertices, edges, MeshType.Triangles, AccessType.Dynamic);
        }

        private Mesh<VertexPC> GenerateSpringMesh()
        {
            if (JelloPoints == null)
                GenerateJelloPoints();

            List<VertexPC> vertices = new List<VertexPC>();
            List<uint> edges = new List<uint>();
            Springs = new List<Spring>();
            var offsetX = -FrameSide / 2;
            var offsetY = -FrameSide / 2;
            var offsetZ = -FrameSide / 2;
            var offset = new Vector3(offsetX, offsetY, offsetZ);
            for (int i = 0; i < SidePoints; i++)
            {
                for (int j = 0; j < SidePoints; j++)
                {
                    for (int k = 0; k < SidePoints; k++)
                    {
                        vertices.Add(new VertexPC(JelloPoints[k, i, j].Model.Position - offset, Color.ColorToVector3()));
                        if (k != 0)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k - 1, i, j]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)((k - 1) + SidePoints * j + SidePoints * SidePoints * i));
                        }
                        if (j != 0)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k, i, j - 1]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k + SidePoints * (j - 1) + SidePoints * SidePoints * i));
                        }
                        if (j != 0 && k < SidePoints - 1)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k + 1, i, j - 1]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k + 1 + SidePoints * (j - 1) + SidePoints * SidePoints * i));
                        }
                        if (j != 0 && k != 0)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k - 1, i, j - 1]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k - 1 + SidePoints * (j - 1) + SidePoints * SidePoints * i));
                        }
                        if (i != 0)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k, i - 1, j]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * (i - 1)));
                        }
                        if (i != 0 && k != 0)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k - 1, i - 1, j]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k - 1 + SidePoints * j + SidePoints * SidePoints * (i - 1)));
                        }
                        if (i != 0 && k < SidePoints - 1)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k + 1, i - 1, j]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k + 1 + SidePoints * j + SidePoints * SidePoints * (i - 1)));
                        }
                        if (i != 0 && j != 0)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k, i - 1, j - 1]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k + SidePoints * (j - 1) + SidePoints * SidePoints * (i - 1)));
                        }
                        if (i != 0 && j < SidePoints - 1)
                        {
                            Springs.Add(AddSping(JelloPoints[k, i, j], JelloPoints[k, i - 1, j + 1]));
                            edges.Add((uint)(k + SidePoints * j + SidePoints * SidePoints * i));
                            edges.Add((uint)(k + SidePoints * (j + 1) + SidePoints * SidePoints * (i - 1)));
                        }
                    }
                }
            }
            _vertices = vertices.ToArray();
            return new Mesh<VertexPC>(_vertices, edges, MeshType.Lines, AccessType.Dynamic);
        }

        public Spring AddSping(JelloPoint one, JelloPoint two)
        {
            var s = new Spring(one, two);
            return s;
        }

        public override void OnUpdate()
        {
        }

        public override void OnRender()
        {
            if (IsJelloVisible)
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                for (var index0 = 0; index0 < JelloPoints.GetLength(0); index0++)
                for (var index1 = 0; index1 < JelloPoints.GetLength(1); index1++)
                for (var index2 = 0; index2 < JelloPoints.GetLength(2); index2++)
                {
                    JelloPoint point = JelloPoints[index2, index1, index0];
                    Shader.Bind(Shader.GetUniformLocation($"points[{index2 + 4 * index1 + 16 * index0}]"), Rotation * point.Model.Position);
                }
                Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
                Shader.Bind(Shader.GetUniformLocation("lightColor"), Scene.SceneLight.Color.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("lightPos"), Scene.SceneLight.Position);
                Shader.Bind(Shader.GetUniformLocation("viewPos"), Scene.SceneCamera.Position);
                Mesh.Draw();
            }
        }
    }
}

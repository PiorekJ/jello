﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Windows.Input;
//using OpenTK;

//namespace JelloProject.Models.Bezier.Curves
//{
//    public abstract class Curve : BaseModel
//    {
//        public virtual ObservableCollection<PointModel> Points { get; protected set; }
//        public bool IsActive
//        {
//            get { return _isActive; }
//            set
//            {
//                _isActive = value;
//                RaisePropertyChanged();
//            }
//        }
//        public bool DisplayBernsteinPoly
//        {
//            get { return _displayBernsteinPolynomial; }
//            set
//            {
//                _displayBernsteinPolynomial = value;
//                RaisePropertyChanged();
//            }
//        }

//        public ICommand ActiveCommand { get; protected set; }
//        public ICommand DeletePointCommand { get; protected set; }

//        private bool _isActive;
//        private bool _displayBernsteinPolynomial;

//        protected Mesh<VertexPC> LinearCurve { get; set; }
//        protected PatchMesh<VertexPC> QuadraticCurve { get; set; }
//        protected PatchMesh<VertexPC> CubicCurve { get; set; }
//        protected Mesh<VertexPC> PolyLines { get; set; }

//        protected Shader LinearShader { get; set; }
//        protected Shader QuadraticShader { get; set; }
//        protected Shader CubicShader { get; set; }
//        protected Shader PolyShader { get; set; }

//        protected virtual List<PointPC> DrawPoints { get; set; }
//        protected VertexPC[] VerticesLinear { get; set; }
//        protected VertexPC[] VerticesQuadratic { get; set; }
//        protected VertexPC[] VerticesCubic { get; set; }
//        protected VertexPC[] VerticesPoly { get; set; }

//        protected abstract void InitializeActiveCommand();

//        public override void Initialize(Simulation simulation)
//        {
//            InitializeActiveCommand();
//            DeletePointCommand = new RelayCommand(param => DeletePoint((PointModel)param));
//            VerticesLinear = new VertexPC[2];
//            VerticesQuadratic = new VertexPC[3];
//            VerticesCubic = new VertexPC[4];
//            Points = new ObservableCollection<PointModel>();
//            DrawPoints = new List<PointPC>();
//            IsMovable = false;
//            base.Initialize(simulation);
//        }

//        protected override void SetShader(ShaderHolder shaderHolder)
//        {
//            LinearShader = shaderHolder.SimpleShader;
//            QuadraticShader = shaderHolder.QuadraticCurveShader;
//            CubicShader = shaderHolder.CubicCurveShader;
//            PolyShader = shaderHolder.SimpleShader;
//        }

//        protected override void SetMesh()
//        {
//            LinearCurve = new Mesh<VertexPC>(VerticesLinear, null, MeshType.Lines, AccessType.Stream);
//            CubicCurve = new PatchMesh<VertexPC>(VerticesCubic, null, 4, AccessType.Stream);
//            QuadraticCurve = new PatchMesh<VertexPC>(VerticesQuadratic, null, 3, AccessType.Stream);
//        }

//        public override void Dispose()
//        {
//            LinearCurve.Dispose();
//            QuadraticCurve.Dispose();
//            CubicCurve.Dispose();
//            PolyLines.Dispose();
//            base.Dispose();
//        }

//        public void SetActive()
//        {
//            IsActive = true;
//        }

//        public void SetInactive()
//        {
//            IsActive = false;
//        }

//        public virtual void AddPoint(PointModel pointToAdd)
//        {
//            pointToAdd.OnRemove += () =>
//            {
//                Points.Remove(pointToAdd);
//                if (Points.Count == 0)
//                    DeleteCommand.Execute(this);
//            };
//            Points.Add(pointToAdd);
//            ReinitializePoly();
//        }

//        public virtual void DeletePoint(PointModel pointToDelete)
//        {
//            Points.Remove(pointToDelete);
//            pointToDelete.DeleteObject(pointToDelete.Mesh);
//            ReinitializePoly();
//        }

//        public virtual void SetPoints(IList<PointModel> points)
//        {
//            foreach (PointModel point in points)
//            {
//                point.OnRemove += () =>
//                {
//                    DeletePoint(point);
//                    if (Points.Count == 0)
//                        DeleteCommand.Execute(this);
//                };
//            }

//            Points = new ObservableCollection<PointModel>(points);
//            ReinitializePoly();
//        }
        
//        protected virtual void CreateDrawPoints()
//        {
//            DrawPoints.Clear();
//            for (int i = 0; i < Points.Count; i++)
//            {
//                DrawPoints.Add(new PointPC(Points[i].Position, Points[i].Color.ConvertToVector3()));
//            }
//        }

//        protected void DrawC0(IList<PointPC> points)
//        {
//            for (int i = 0; i < (points.Count - 1) / 3; i++)
//            {
//                for (int j = 0; j < 4; j++)
//                {
//                    VerticesCubic[j] = new VertexPC(points[3 * i + j].Position, points[3 * i + j].Color);
//                }

//                CubicCurve.SetVertices(0, VerticesCubic);

//                CubicShader.Activate();
//                CubicShader.Bind(CubicShader.GetUniformLocation("screenSize"),
//                    new Vector2(Constants.ScreenSizeX, Constants.ScreenSizeY));
//                CubicShader.Bind(CubicShader.GetUniformLocation("view"), SimulationRef.SceneCamera.GetViewMatrix());
//                CubicShader.Bind(CubicShader.GetUniformLocation("projection"), SimulationRef.SceneCamera.GetProjectionMatrix());
//                CubicCurve.Draw();
//            }

//            switch ((points.Count - 1) % 3)
//            {
//                case 1:
//                    VerticesLinear[0] = new VertexPC(points[points.Count - 2].Position, points[points.Count - 2].Color);
//                    VerticesLinear[1] = new VertexPC(points[points.Count - 1].Position, points[points.Count - 1].Color);
//                    LinearCurve.SetVertices(0, VerticesLinear);
//                    LinearShader.Activate();
//                    LinearShader.Bind(LinearShader.GetUniformLocation("model"), Matrix4.Identity);
//                    LinearShader.Bind(LinearShader.GetUniformLocation("view"), SimulationRef.SceneCamera.GetViewMatrix());
//                    LinearShader.Bind(LinearShader.GetUniformLocation("projection"),
//                        SimulationRef.SceneCamera.GetProjectionMatrix());
//                    LinearCurve.Draw();
//                    break;
//                case 2:
//                    VerticesQuadratic[0] = new VertexPC(points[points.Count - 3].Position, points[points.Count - 3].Color);
//                    VerticesQuadratic[1] = new VertexPC(points[points.Count - 2].Position, points[points.Count - 2].Color);
//                    VerticesQuadratic[2] = new VertexPC(points[points.Count - 1].Position, points[points.Count - 1].Color);
//                    QuadraticCurve.SetVertices(0, VerticesQuadratic);
//                    QuadraticShader.Activate();
//                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("screenSize"),
//                        new Vector2(Constants.ScreenSizeX, Constants.ScreenSizeY));
//                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("view"), SimulationRef.SceneCamera.GetViewMatrix());
//                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("projection"),
//                        SimulationRef.SceneCamera.GetProjectionMatrix());
//                    QuadraticCurve.Draw();
//                    break;
//            }
//        }

//        protected void DrawBernstainPoly()
//        {
//            if (DisplayBernsteinPoly)
//            {
//                for (int i = 0; i < Points.Count; i++)
//                {
//                    VerticesPoly[i] = new VertexPC(Points[i].Position, new Vector3(Points[i].Color.ScR, Points[i].Color.ScG, Points[i].Color.ScB));
//                }
//                PolyLines.SetVertices(0, VerticesPoly);
//                PolyShader.Activate();
//                PolyShader.Bind(LinearShader.GetUniformLocation("model"), Matrix4.Identity);
//                PolyShader.Bind(LinearShader.GetUniformLocation("view"), SimulationRef.SceneCamera.GetViewMatrix());
//                PolyShader.Bind(LinearShader.GetUniformLocation("projection"), SimulationRef.SceneCamera.GetProjectionMatrix());
//                PolyLines.Draw();
//            }
//        }

//        protected void ReinitializePoly()
//        {
//            VerticesPoly = new VertexPC[Points.Count];
//            uint[] edgesPoly = new uint[Points.Count*2 - 2];
//            for (int i = 0; i < Points.Count; i++)
//            {
//                VerticesPoly[i] = new VertexPC(Points[i].Position, new Vector3(Points[i].Color.ScR, Points[i].Color.ScG, Points[i].Color.ScB));
//            }
//            for (int i = 1; i < edgesPoly.Length + 1; i++)
//            {
//                edgesPoly[i - 1] = (uint) Math.Floor((double) i/2);
//            }
//            PolyLines?.Dispose();
//            PolyLines = new Mesh<VertexPC>(VerticesPoly, edgesPoly, MeshType.Lines, AccessType.Stream);
//        }
//    }
//}

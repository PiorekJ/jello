﻿using System;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.OpenTK;

namespace JelloProject.Models
{
    public interface IColorable
    {
        Color Color { get; set; }
    }

    public abstract class Model : SceneObject, IDisposable
    {
        public IMesh Mesh;
        public Shader Shader;

        public abstract void SetMesh();
        public abstract void SetShader();

        protected Model(Scene currScene) : base(currScene)
        {
        }

        public void Dispose()
        {
            Mesh?.Dispose();
            Shader?.Dispose();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JelloProject.Models;
using JelloProject.Utils;
using OpenTK;

namespace JelloProject.Core.JelloCore
{
    public class JelloPoint : BindableObject
    {
        public ScenePoint Model;
        public Vector3 Velocity = Vector3.Zero;
        public Vector3 Force;
        public Vector3 Acceleration;
        
        private SceneCollisionBox _collisionBox;
        public JelloPoint(Vector3 position, Scene scene)
        {
            Model = scene.AddPoint(position);
            _collisionBox = scene.CollisionBox;
        }

        public void Integrate(float drag, float mass)
        {
            Force -= drag * Velocity;

            Acceleration = Force / mass;
            
            Velocity += Acceleration * Simulation.IntegrationStep;
            Velocity = _collisionBox.Collide(Model.Position + Velocity * Simulation.IntegrationStep, Velocity);
            Model.Position += Velocity * Simulation.IntegrationStep;
        }

        public void ClearForce()
        {
            Force = Vector3.Zero;
        }
    }
}

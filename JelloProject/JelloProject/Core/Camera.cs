﻿using System;
using JelloProject.Utils;
using OpenTK;
using Key = System.Windows.Input.Key;
using MouseButton = System.Windows.Input.MouseButton;

namespace JelloProject.Core
{
    public class Camera : SceneObject
    {
        public float FOV { get; set; } = Settings.DefaultFOV;
        public float ZNear { get; set; } = Settings.DefaultFOV;
        public float ZFar { get; set; } = Settings.DefaultZFar;

        private Vector2 _lastMousePosition = Vector2.Zero;

        public float RotationX
        {
            get { return _rotationX; }
            set
            {
                _rotationX = value;
                RaisePropertyChanged();
            }
        }

        public float RotationY
        {
            get { return _rotationY; }
            set
            {
                _rotationY = value;
                RaisePropertyChanged();
            }
        }


        private float _aspectRatio;
        private float _rotationX;
        private float _rotationY;

        public bool IsMoving => InputManager.IsMouseButtonDown(MouseButton.Middle);

        public bool IsRotating => InputManager.IsMouseButtonDown(MouseButton.Right);



        public Matrix4 GetViewMatrix()
        {
            return Matrix4.CreateTranslation(-Position) * Matrix4.CreateFromQuaternion(Rotation.Inverted());
        }

        public Matrix4 GetProjectionMatrix()
        {
            return Matrix4.CreatePerspectiveFieldOfView(FOV, _aspectRatio, ZNear, ZFar);
        }

        public void SetCurrentAspectRatio(float aspectRatio)
        {
            _aspectRatio = aspectRatio;
        }

        public Camera(Scene currScene) : base(currScene)
        {
            Position = Settings.DefaultCameraPosition;
            RotationX = Settings.DefaultCameraRotation.X;
            RotationY = Settings.DefaultCameraRotation.Z;
            Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                       Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
            InputManager.OnMouseScrollEvent += Zoom;
        }

        public void UpdateCamera()
        {
            KeyboardControl();
            MouseControl(InputManager.MousePosition);
        }

        private void MouseControl(Vector2 mousePosition)
        {
            if (_lastMousePosition == mousePosition)
                return;

            var diffX = mousePosition.X - _lastMousePosition.X;
            var diffY = mousePosition.Y - _lastMousePosition.Y;

            _lastMousePosition = mousePosition;

            if (IsMoving)
            {
                Vector3 top = Vector3.Transform(-Vector3.UnitY, Rotation);
                Vector3 right = Vector3.Transform(Vector3.UnitX, Rotation);

                Position -= (top * diffY + right * diffX) * Settings.CameraMovementMouseSensitivity * Simulation.DeltaTime;
            }

            if (IsRotating)
            {
                RotationX -= diffY * Settings.CameraRotationMouseSensitivity * Simulation.DeltaTime;
                RotationY -= diffX * Settings.CameraRotationMouseSensitivity * Simulation.DeltaTime;

                Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                           Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
            }
        }

        private void Zoom(int delta)
        {
            if (InputManager.IsKeyDown(Settings.MoveFrameKeybinding) || InputManager.IsKeyDown(Settings.ZoomFrameKeybinding))
                return;
            if (delta < 0)
                Position += Rotation * Vector3.UnitZ * Settings.CameraZoomMouseSensitivity;
            if (delta > 0)
                Position -= Rotation * Vector3.UnitZ * Settings.CameraZoomMouseSensitivity;
        }

        private void KeyboardControl()
        {
            float velocity = 0;
            if (InputManager.IsKeyDown(Key.LeftShift))
            {
                velocity = Settings.CameraMovementKeySlowVelocity;
            }
            else
            {
                velocity = Settings.CameraMovementKeyVelocity;
            }

            if (InputManager.IsKeyDown(Key.W))
            {
                Position -= Rotation * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }
            if (InputManager.IsKeyDown(Key.S))
            {
                Position += Rotation * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }
            if (InputManager.IsKeyDown(Key.A))
            {
                Position -= Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }
            if (InputManager.IsKeyDown(Key.D))
            {
                Position += Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.Space))
            {
                Position += Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.LeftCtrl))
            {
                Position -= Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }
        }

        public void SetLastMousePosition(Vector2 pos)
        {
            _lastMousePosition = pos;
        }

        public Rayline CalculateCameraScreenRayLine(Vector2 screenPoint, int viewportWidth, int viewportHeight)
        {
            Vector4 screenPosNorm = new Vector4(screenPoint.X / viewportWidth * 2 - 1, -(screenPoint.Y / viewportHeight * 2 - 1), 0, 1);
            screenPosNorm = screenPosNorm * Matrix4.Invert(GetProjectionMatrix()) * Matrix4.Invert(GetViewMatrix());
            Vector3 screenVector3 = new Vector3(screenPosNorm.X / screenPosNorm.W, screenPosNorm.Y / screenPosNorm.W,
                screenPosNorm.Z / screenPosNorm.W);
            Vector3 cameraLookDir = screenVector3 - Position;
            return new Rayline(screenVector3, Vector3.Normalize(cameraLookDir));
        }

        public Rayline GetCameraCenterRayline()
        {
            return Scene.SceneCamera.CalculateCameraScreenRayLine(new Vector2(Settings.DisplayWidth / 2f, Settings.DisplayHeigth / 2f),
                Settings.DisplayWidth, Settings.DisplayHeigth);
        }
    }
}

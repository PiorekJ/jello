﻿using System;
using System.Windows.Input;
using OpenTK;

namespace JelloProject.Utils
{
    public static class Settings
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 0.5f, 5);
        public static Vector3 DefaultCameraRotation = new Vector3(MathHelper.DegreesToRadians(-5), 0, 0);
        public const float DefaultFOV = (float)Math.PI / 4;
        public const float DefaultZNear = 0.01f;
        public const float DefaultZFar = 50f;

        public const float CameraMovementMouseSensitivity = 0.5f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 0.5f;


        public const float CameraMovementKeyVelocity = 5.0f;
        public const float CameraMovementKeySlowVelocity = 0.25f;

        public const float Epsilon = 0.00000001f;

        public const float JelloSide = 1.0f;

        public static int DisplayWidth;
        public static int DisplayHeigth;

        public static readonly Key MoveFrameKeybinding = Key.M;
        public static readonly Key ZoomFrameKeybinding = Key.Z;
        public static readonly Key RotateFrameKeybinding = Key.R;
        public const float FrameZoomMouseSensitivity = 0.2f;

    }
}

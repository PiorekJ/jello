﻿using System.Collections.Generic;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.OpenTK;
using OpenTK;

namespace JelloProject.Models
{
    public class SceneLight : Model
    {
        public Color Color { get; set; } = Colors.White;

        public SceneLight(Scene currScene) : base(currScene)
        {
            Position = new Vector3(0, 5, 0);
            Scale = new Vector3(0.25f);
            Shader = Shaders.LightShader;
            Mesh = GenerateCubeMesh();
        }



        public override void OnUpdate()
        {
        }

        public override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Mesh.Draw();
        }

        public override void SetMesh()
        {
            Mesh = GenerateCubeMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.LightShader;
        }


        private Mesh<VertexP> GenerateCubeMesh()
        {
            List<VertexP> vertices = new List<VertexP>()
            {
                new VertexP(-1.0f, -1.0f, 1.0f),
                new VertexP(1.0f, -1.0f, 1.0f),
                new VertexP(1.0f, 1.0f, 1.0f),
                new VertexP(-1.0f, 1.0f, 1.0f),
                new VertexP(-1.0f, -1.0f, -1.0f),
                new VertexP(1.0f, -1.0f, -1.0f),
                new VertexP(1.0f, 1.0f, -1.0f),
                new VertexP(-1.0f, 1.0f, -1.0f)
            };
            List<uint> edges = new List<uint>()
            {
                0,
                1,
                2,
                2,
                3,
                0,

                1,
                5,
                6,
                6,
                2,
                1,

                7,
                6,
                5,
                5,
                4,
                7,

                4,
                0,
                3,
                3,
                7,
                4,

                4,
                5,
                1,
                1,
                0,
                4,

                3,
                2,
                6,
                6,
                7,
                3
            };

            return new Mesh<VertexP>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }
    }
}

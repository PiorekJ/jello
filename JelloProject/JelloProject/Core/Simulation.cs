﻿using System;
using System.Linq;
using System.Linq.Expressions;
using JelloProject.Core.JelloCore;
using JelloProject.Utils;
using OpenTK;
using Assimp;
using Assimp.Configs;
namespace JelloProject.Core
{
    public class Simulation : BindableObject
    {
        public Scene Scene
        {
            get { return _scene; }
            set
            {
                _scene = value;
                RaisePropertyChanged();
            }
        }

        public TimeCounter Time;

        public bool IsRunning = false;

        public static float IntegrationStep = 1 / 100f;
        public static float IntegrationStepSquared = IntegrationStep * IntegrationStep;

        public static float DeltaTime => TimeCounter.DeltaTime;

        #region Settings

        public float ViscousDrag
        {
            get => _viscousDrag;
            set
            {
                _viscousDrag = value;
                RaisePropertyChanged();
            }
        }

        public float JelloSpringElasticity
        {
            get => _jelloSpringElasticity;
            set
            {
                _jelloSpringElasticity = value;
                RaisePropertyChanged();
            }
        }

        public float FrameSpringElasticity
        {
            get => _frameSpringElasticity;
            set
            {
                _frameSpringElasticity = value;
                RaisePropertyChanged();
            }
        }

        public float Mass
        {
            get => _mass / 64f;
            set
            {
                _mass = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 InitialVelocityMin
        {
            get => _initialVelocityMin;
            set
            {
                _initialVelocityMin = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 InitialVelocityMax
        {
            get => _initialVelocityMax;
            set
            {
                _initialVelocityMax = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 InitialPositionMin
        {
            get => _initialPositionMin;
            set
            {
                _initialPositionMin = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 InitialPositionMax
        {
            get => _initialPositionMax;
            set
            {
                _initialPositionMax = value;
                RaisePropertyChanged();
            }
        }


        private float _viscousDrag = 0.1f;
        private float _jelloSpringElasticity = 2f;
        private float _frameSpringElasticity = 10f;
        private float _mass = 1;
        private Vector3 _initialVelocityMin;
        private Vector3 _initialVelocityMax;
        private Vector3 _initialPositionMin;
        private Vector3 _initialPositionMax;
        private Scene _scene;

        #endregion


        public Simulation()
        {
            Scene = new Scene();
            Time = new TimeCounter();
        }

        public void InitializeSimulation()
        {
            Time.Start();
            Scene.AddSceneLight();
            Scene.AddSceneCursor();
            Scene.AddCollisionBoxModel();
            //Scene.AddSceneTestModel();
            Scene.AddJello();
            Scene.AddJelloFrame();
            Scene.AddFloorGrid().Position = -2.5f * Vector3.UnitY;
        }

        public void AddRandomVelocities()
        {
            var rand = new Random();
            
            foreach (JelloPoint jelloPoint in Scene.SceneJello.JelloPoints)
            {
                jelloPoint.Velocity = new Vector3((float)rand.NextDouble() * (InitialVelocityMax.X - InitialVelocityMin.X) + InitialVelocityMin.X, (float)rand.NextDouble() * (InitialVelocityMax.Y - InitialVelocityMin.Y) + InitialVelocityMin.Y, (float)rand.NextDouble() * (InitialVelocityMax.Z - InitialVelocityMin.Z) + InitialVelocityMin.Z);
            }
        }

        public void AddRandomPositions()
        {
            var rand = new Random();

            foreach (JelloPoint jelloPoint in Scene.SceneJello.JelloPoints)
            {
                jelloPoint.Model.Position = new Vector3((float)rand.NextDouble() * (InitialPositionMax.X - InitialPositionMin.X) + InitialPositionMin.X, (float)rand.NextDouble() * (InitialPositionMax.Y - InitialPositionMin.Y) + InitialPositionMin.Y, (float)rand.NextDouble() * (InitialPositionMax.Z - InitialPositionMin.Z) + InitialPositionMin.Z);
            }
        }

        public void Simulate()
        {
            foreach (JelloPoint point in Scene.SceneJello.JelloPoints)
            {
                point.ClearForce();
            }

            foreach (Spring spring in Scene.SceneJelloFrame.FrameSprings)
            {
                spring.CalculateSpringForce(FrameSpringElasticity);
            }

            foreach (Spring spring in Scene.SceneJello.Springs)
            {
                spring.CalculateSpringForce(JelloSpringElasticity);
            }

            foreach (JelloPoint point in Scene.SceneJello.JelloPoints)
            {
                point.Integrate(ViscousDrag, Mass);
            }
        }





    }
}

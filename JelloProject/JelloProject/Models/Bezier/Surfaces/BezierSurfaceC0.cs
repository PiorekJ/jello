﻿//using System;
//using JelloProject.Core;
//using OpenTK;
//using Vector3 = OpenTK.Vector3;

//namespace JelloProject.Models.Bezier.Surfaces
//{
//    public class BezierSurfaceC0 : BezierSurface
//    {
//        protected override void SetShader(Shaders shaderHolder)
//        {
//            Shader = shaderHolder.BezierSurfaceC0Shader;
//        }

//        protected override void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY)
//        {
//            int pointsX = 3 * patchesCountX + 1;
//            int pointsY = 3 * patchesCountY + 1;

//            float distX = width / (pointsX - 1);
//            float distY = height / (pointsY - 1);
//            for (int j = 0; j < pointsY; j++)
//            {
//                for (int i = 0; i < pointsX; i++)
//                {
//                    PointModel point = new PointModel();
//                    point.Position = new Vector3(i * distX, 0, -j * distY);   
//                    point.Initialize(SimulationRef);
//                    SimulationRef.AddSceneObject(point);
//                    SurfacePoints.Add(point);
//                }
//            }
//        }

//        protected override void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY)
//        {
//            for (int i = 0; i < patchesCountY; i++)
//            {
//                for (int j = 0; j < patchesCountX; j++)
//                {
//                    BezierSurfacePatch bezierSurfacePatch = new BezierSurfacePatch(j, i);
//                    int cornerIndexX = j * 3;
//                    int cornerIndexY = i * 3;
//                    for (int k = 0; k < 4; k++)
//                    {
//                        for (int l = 0; l < 4; l++)
//                        {
//                            int pointX = cornerIndexX + l;
//                            int pointY = cornerIndexY + k;
//                            bezierSurfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (3 * patchesCountX + 1)]);
//                        }
//                    }
//                    SurfacePatches.Add(bezierSurfacePatch);
//                }
//            }
//        }
//    }
//}

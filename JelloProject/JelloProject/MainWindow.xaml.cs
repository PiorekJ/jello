﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.Models;
using JelloProject.Utils;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using InputManager = JelloProject.Core.InputManager;

namespace JelloProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }

        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetCurrentAspectRatio(DisplayControl.AspectRatio);
            Settings.DisplayWidth = DisplayControl.ControlWidth;
            Settings.DisplayHeigth = DisplayControl.ControlHeigth;
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);

            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
            GL.LineWidth(1);

            Simulation.InitializeSimulation();

            CompositionTarget.Rendering += OnRender;
        }

        private int physUpdates;
        private double restFrame = 0;
        private double physTimeFrame;
        private void OnRender(object sender, EventArgs e)
        {
            Simulation.Time.CountDT();
            physTimeFrame += Simulation.DeltaTime;
            GL.ClearColor(System.Drawing.Color.Black);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            InputManager.SetMousePosition(DisplayControl.MousePosition);

            if(!InputManager.IsKeyDown(Settings.MoveFrameKeybinding))
                Simulation.Scene.SceneCamera.UpdateCamera();
            
           

            physUpdates = (int)(physTimeFrame / Simulation.DeltaTime);

            if (Simulation.IsRunning)
                for (int i = 0; i < physUpdates; i++)
                {
                    Simulation.Simulate();
                }

            foreach (Model model in Simulation.Scene.SceneModels)
            {
                model.OnRender();
                model.OnUpdate();
            }

            physTimeFrame -= physUpdates * Simulation.DeltaTime;

            DisplayControl.SwapBuffers();
        }

        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetCurrentAspectRatio(DisplayControl.AspectRatio);
            Settings.DisplayWidth = DisplayControl.ControlWidth;
            Settings.DisplayHeigth = DisplayControl.ControlHeigth;
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (Model model in Simulation.Scene.SceneModels)
            {
                model.Dispose();
            }
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
            
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }

        private void StartButton_OnClick(object sender, RoutedEventArgs e)
        {
            if((Simulation.InitialVelocityMin != Vector3.Zero || Simulation.InitialVelocityMax != Vector3.Zero) && !Simulation.IsRunning)
                Simulation.AddRandomVelocities();

            if ((Simulation.InitialPositionMin != Vector3.Zero || Simulation.InitialPositionMax != Vector3.Zero) && !Simulation.IsRunning)
                Simulation.AddRandomPositions();

            Simulation.IsRunning = true;
            
        }

        private void StopButton_OnClick(object sender, RoutedEventArgs e)
        {
            Simulation.IsRunning = false;
        }
    }
}

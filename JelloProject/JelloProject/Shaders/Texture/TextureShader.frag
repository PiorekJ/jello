﻿#version 330 core
out layout(location = 0) vec4 FragColor;

in V_OUT 
{
	vec2 texCoord;
} In;

uniform sampler2D tex;

void main()
{
	FragColor = texture(tex, In.texCoord);
}

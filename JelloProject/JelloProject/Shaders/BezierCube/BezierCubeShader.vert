﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 normal;
in layout(location = 2) vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 points[64];  

mat4 projectionViewModel = projection * view * model;

out V_OUT
{
	vec3 normal;
	vec3 color;
	vec3 fragPos;
} Out;

vec3 deCasteljau(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
	float paramT = (1-t);
	
	a = a * paramT + b*t;
	b = b * paramT + c*t;
	c = c * paramT + d*t;

	a = a * paramT + b*t;
	b = b * paramT + c*t;
	
	a = a * paramT + b*t;

	return a;
}

vec3 surfaceCalc(vec3 positionUVW)
{
	vec3 sumL1 = deCasteljau(positionUVW.z, points[0], points[16], points[32], points[48]);
	vec3 sumL2 = deCasteljau(positionUVW.z, points[1], points[17], points[33], points[49]);
	vec3 sumL3 = deCasteljau(positionUVW.z, points[2], points[18], points[34], points[50]);
	vec3 sumL4 = deCasteljau(positionUVW.z, points[3], points[19], points[35], points[51]);
	vec3 sumL5 = deCasteljau(positionUVW.z, points[4], points[20], points[36], points[52]);
	vec3 sumL6 = deCasteljau(positionUVW.z, points[5], points[21], points[37], points[53]);
	vec3 sumL7 = deCasteljau(positionUVW.z, points[6], points[22], points[38], points[54]);
	vec3 sumL8 = deCasteljau(positionUVW.z, points[7], points[23], points[39], points[55]);
	vec3 sumL9 = deCasteljau(positionUVW.z, points[8], points[24], points[40], points[56]);
	vec3 sumL10= deCasteljau(positionUVW.z, points[9], points[25], points[41], points[57]);
	vec3 sumL11 = deCasteljau(positionUVW.z, points[10], points[26], points[42], points[58]);
	vec3 sumL12 = deCasteljau(positionUVW.z, points[11], points[27], points[43], points[59]);
	vec3 sumL13 = deCasteljau(positionUVW.z, points[12], points[28], points[44], points[60]);
	vec3 sumL14 = deCasteljau(positionUVW.z, points[13], points[29], points[45], points[61]);
	vec3 sumL15 = deCasteljau(positionUVW.z, points[14], points[30], points[46], points[62]);
	vec3 sumL16 = deCasteljau(positionUVW.z, points[15], points[31], points[47], points[63]);

	vec3 sumLL1 = deCasteljau(positionUVW.y, sumL1, sumL5, sumL9, sumL13);
	vec3 sumLL2 = deCasteljau(positionUVW.y, sumL2, sumL6, sumL10, sumL14);
	vec3 sumLL3 = deCasteljau(positionUVW.y, sumL3, sumL7, sumL11, sumL15);
	vec3 sumLL4 = deCasteljau(positionUVW.y, sumL4, sumL8, sumL12, sumL16);

	vec3 sumR = deCasteljau(positionUVW.x, sumLL1, sumLL2, sumLL3, sumLL4);
	
	return sumR;
}

void main()
{
	vec3 surfacePoint = surfaceCalc(position);
	Out.fragPos = vec3(model * vec4(surfacePoint, 1));
	Out.normal = mat3(transpose(inverse(model))) * normal;
	Out.color = color;
	
	gl_Position = projection * view * vec4(Out.fragPos, 1.0);
}
﻿//using System.Linq;

//namespace JelloProject.Models.Bezier.Curves
//{
//    public class CurveC0 : Curve
//    {
//        public override void Draw()
//        {
//            if (Points.Count < 2)
//                return;

//            CreateDrawPoints();
//            DrawC0(DrawPoints);
//            DrawBernstainPoly();
//        }

//        protected override void InitializeActiveCommand()
//        {
//            ActiveCommand = new RelayCommand(param => SimulationRef.SetActiveCurve(this));
//        }

//        public GM1.Serialization.BezierCurveC0 SerializeC0()
//        {
//            return new BezierCurveC0() { Name = ObjectName, DisplayPolygon = DisplayBernsteinPoly, DisplayPolygonSpecified = true, Points = Points.Select(point => SimulationRef.SceneObjects.IndexOf(point)).ToArray() };
//        }
//    }
//}

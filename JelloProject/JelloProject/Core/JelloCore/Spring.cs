﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JelloProject.Properties;
using OpenTK;
using Settings = JelloProject.Utils.Settings;

namespace JelloProject.Core.JelloCore
{
    public class Spring
    {
        public JelloPoint Point1;
        public JelloPoint Point2;

        public float InitialLength;

        public float I;

        public Spring(JelloPoint point1, JelloPoint point2)
        {
            Point1 = point1;
            Point2 = point2;
            InitialLength = (Point2.Model.Position - Point1.Model.Position).Length;
        }

        public void CalculateSpringForce(float elasticity)
        {
            var vect = Point2.Model.Position - Point1.Model.Position;
            var dist = vect.Length;
            var dir = Vector3.UnitX;
            if (Math.Abs(dist) > Settings.Epsilon)
                dir = vect/dist;

            var i = dist - InitialLength;

            var f = dir * elasticity * i;

            Point1.Force += f;
            Point2.Force -= f;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.OpenTK;
using JelloProject.Utils;
using OpenTK;

namespace JelloProject.Models
{
    public class SceneCollisionBox : Model, IColorable
    {
        public Color Color { get; set; } = Colors.White;

        public Vector3 CollisionBoxSize = new Vector3(10, 5, 10);
        private float _collisionCoefficient = 0f;
        private bool _isVisible = true;

        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                RaisePropertyChanged();
            }
        }

        public float CollisionCoefficient
        {
            get => _collisionCoefficient;
            set
            {
                _collisionCoefficient = value;
                RaisePropertyChanged();
            }
        }

        public SceneCollisionBox(Scene currScene) : base(currScene)
        {
            Mesh = GenerateFrameMesh();
            Shader = Shaders.BasicShader;
        }

        public override void SetMesh()
        {
            Mesh = GenerateFrameMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        public override void OnRender()
        {
            if (IsVisible)
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                Mesh.Draw();
            }
        }

        private IMesh GenerateFrameMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            List<uint> edges = new List<uint>();

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        vertices.Add(new VertexPC(CollisionBoxSize.X * k - CollisionBoxSize.X / 2, CollisionBoxSize.Y * i - CollisionBoxSize.Y / 2, CollisionBoxSize.Z * j - CollisionBoxSize.Z / 2, Color.ColorToVector3()));
                        if (k != 0)
                        {
                            edges.Add((uint)(k + 2 * j + 4 * i));
                            edges.Add((uint)((k - 1) + 2 * j + 4 * i));
                        }
                        if (j != 0)
                        {
                            edges.Add((uint)(k + 2 * j + 4 * i));
                            edges.Add((uint)(k + 2 * (j - 1) + 4 * i));
                        }
                        if (i != 0)
                        {
                            edges.Add((uint)(k + 2 * j + 4 * i));
                            edges.Add((uint)(k + 2 * j + 4 * (i - 1)));
                        }
                    }
                }
            }
            return new Mesh<VertexPC>(vertices, edges, MeshType.Lines, AccessType.Static);
        }

        public Vector3 Collide(Vector3 nextPosition, Vector3 velocity)
        {
            if (Math.Abs(nextPosition.X) > CollisionBoxSize.X / 2)
            {
                velocity.X = -CollisionCoefficient * velocity.X;
            }
            if (Math.Abs(nextPosition.Y) > CollisionBoxSize.Y / 2)
            {
                velocity.Y = -CollisionCoefficient * velocity.Y;
            }
            if (Math.Abs(nextPosition.Z) > CollisionBoxSize.Z / 2)
            {
                velocity.Z = -CollisionCoefficient * velocity.Z;
            }

            return velocity;
        }
    }
}

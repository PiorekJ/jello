﻿#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 texCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 projectionViewModel = projection*view*model;

out V_OUT
{
	vec2 texCoord;
} Out;


void main()
{
	gl_Position = projectionViewModel * vec4(pos,1);
	Out.texCoord = texCoord;
}


﻿using System.Collections.Generic;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.Core.JelloCore;
using JelloProject.OpenTK;
using JelloProject.Utils;
using OpenTK;

namespace JelloProject.Models
{
    public class SceneTestModel : Model
    {
        public Color Color { get; set; } = Colors.LawnGreen;

        public SceneTestModel(Scene currScene) : base(currScene)
        {
            Position = new Vector3(0, 0, 0);
            Shader = Shaders.LitObjectShader;
            Mesh = AssimpManager.LoadMesh("./OBJModels/monkey.obj");//GenerateCubeMesh();
        }



        public override void OnUpdate()
        {
        }

        public override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            Shader.Bind(Shader.GetUniformLocation("lightColor"), Scene.SceneLight.Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("lightPos"), Scene.SceneLight.Position);
            Shader.Bind(Shader.GetUniformLocation("viewPos"), Scene.SceneCamera.Position);
            Mesh.Draw();
        }

        public override void SetMesh()
        {
            Mesh = AssimpManager.LoadMesh("./OBJModels/monkey.obj");
        }

        public override void SetShader()
        {
            Shader = Shaders.LitObjectShader;
        }


        private Mesh<VertexPNC> GenerateCubeMesh()
        {
            List<VertexPNC> vertices = new List<VertexPNC>()
            {
                new VertexPNC(-1, -1, -1,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1, -1,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1, -1,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1, -1,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(-1,  1, -1,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1, -1,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),

                new VertexPNC(-1, -1,  1,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1,  1,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1,  1,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1,  1,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(-1,  1,  1,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1,  1,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),

                new VertexPNC(-1,  1,  1, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1,  1, -1, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1, -1, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1, -1, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1,  1, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1,  1,  1, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(1,  1, -1,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1,  1,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1, -1,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1, -1,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1,  1,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1,  1,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(-1, -1, -1,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1, -1,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1,  1,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1, -1,  1,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1,  1,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1, -1, -1,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(-1,  1, -1,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1, -1,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1,  1,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(1,  1,  1,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1,  1,  1,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-1,  1, -1,  0.0f,  1.0f,  0.0f, Color.ColorToVector3())
            };

            return new Mesh<VertexPNC>(vertices, null, MeshType.Triangles, AccessType.Static);
        }
    }
}

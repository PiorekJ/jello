﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.Core.JelloCore;
using JelloProject.OpenTK;
using JelloProject.Utils;
using JelloProject.Utils.LogicModels;
using OpenTK;
using InputManager = JelloProject.Core.InputManager;

namespace JelloProject.Models
{
    public class SceneJelloFrame : Model, IColorable
    {
        public Color Color { get; set; } = Colors.Yellow;
        private const float FrameSide = Settings.JelloSide;
        public JelloPoint[,,] FramePoints;
        public List<Spring> FrameSprings;

        private VertexPC[] _vertices;
        private readonly Plane _movementPlane;
        private Vector3 _movementIntersect;
        private Vector3 _initialPosition;
        private Vector2 _lastMousePosition;
        private bool _isVisible = true;
        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                _isVisible = value;
                RaisePropertyChanged();
                foreach (JelloPoint point in FramePoints)
                {
                    point.Model.IsVisible = false;
                }
            }
        }

        public SceneJelloFrame(Scene currScene) : base(currScene)
        {
            FramePoints = new JelloPoint[2, 2, 2];
            FrameSprings = new List<Spring>();
            Mesh = GenerateFrameMesh();
            Shader = Shaders.BasicShader;
            _movementPlane = new Plane();
            _movementIntersect = new Vector3();

            InputManager.RegisterOnKeyDownEvent(Settings.MoveFrameKeybinding, BeginMovementHandler);
            InputManager.RegisterOnKeyDownEvent(Settings.RotateFrameKeybinding, BeginRotationHandler);
            InputManager.OnMouseScrollEvent += ZoomFrame;
        }

        private void ZoomFrame(int delta)
        {
            if (!InputManager.IsKeyDown(Settings.ZoomFrameKeybinding) || InputManager.IsKeyDown(Settings.MoveFrameKeybinding))
                return;
            SetMovementPlane();
            if (delta > 0)
                Position -= _movementPlane.Normal.Normalized() * Settings.FrameZoomMouseSensitivity;
            if (delta < 0)
                Position += _movementPlane.Normal.Normalized() * Settings.FrameZoomMouseSensitivity;

        }

        private void BeginMovementHandler()
        {
            SetMovementPlane();
            var mouseRayline = Scene.SceneCamera.CalculateCameraScreenRayLine(InputManager.MousePosition,
                Settings.DisplayWidth, Settings.DisplayHeigth);
            _movementIntersect = _movementPlane.CalculateRayIntersetion(mouseRayline);
            _initialPosition = Position;
        }

        private void BeginRotationHandler()
        {
            SetMovementPlane();
        }

        private void SetMovementPlane()
        {
            var centerRayline = Scene.SceneCamera.GetCameraCenterRayline();
            _movementPlane.Origin = Position;
            _movementPlane.Normal = -centerRayline.Direction;
        }

        public override void SetMesh()
        {
            Mesh = GenerateFrameMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        float offsetX = -FrameSide / 2;
        float offsetY = -FrameSide / 2;
        float offsetZ = -FrameSide / 2;
        private Mesh<VertexPC> GenerateFrameMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            List<uint> edges = new List<uint>();

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        FramePoints[k, i, j] = new JelloPoint(new Vector3(FrameSide * k + offsetX, FrameSide * i + offsetY, FrameSide * j + offsetZ), Scene);
                        vertices.Add(new VertexPC(FramePoints[k, i, j].Model.Position, Color.ColorToVector3()));
                        if (k != 0)
                        {
                            edges.Add((uint)(k + 2 * j + 4 * i));
                            edges.Add((uint)((k - 1) + 2 * j + 4 * i));
                        }
                        if (j != 0)
                        {
                            edges.Add((uint)(k + 2 * j + 4 * i));
                            edges.Add((uint)(k + 2 * (j - 1) + 4 * i));
                        }
                        if (i != 0)
                        {
                            edges.Add((uint)(k + 2 * j + 4 * i));
                            edges.Add((uint)(k + 2 * j + 4 * (i - 1)));
                        }
                    }
                }
            }

            if (Scene.SceneJello != null)
            {
                var jello = Scene.SceneJello;

                FrameSprings.Add(AddSping(jello?.JelloPoints[0, 0, 0], FramePoints[0, 0, 0]));
                FrameSprings.Add(AddSping(jello?.JelloPoints[jello.SidePoints - 1, 0, 0], FramePoints[1, 0, 0]));
                FrameSprings.Add(AddSping(jello?.JelloPoints[0, 0, jello.SidePoints - 1], FramePoints[0, 0, 1]));
                FrameSprings.Add(AddSping(jello?.JelloPoints[jello.SidePoints - 1, 0, jello.SidePoints - 1], FramePoints[1, 0, 1]));

                FrameSprings.Add(AddSping(jello?.JelloPoints[0, jello.SidePoints - 1, 0], FramePoints[0, 1, 0]));
                FrameSprings.Add(AddSping(jello?.JelloPoints[jello.SidePoints - 1, jello.SidePoints - 1, 0], FramePoints[1, 1, 0]));
                FrameSprings.Add(AddSping(jello?.JelloPoints[0, jello.SidePoints - 1, jello.SidePoints - 1], FramePoints[0, 1, 1]));
                FrameSprings.Add(AddSping(jello?.JelloPoints[jello.SidePoints - 1, jello.SidePoints - 1, jello.SidePoints - 1], FramePoints[1, 1, 1]));
            }

            _vertices = vertices.ToArray();

            return new Mesh<VertexPC>(_vertices, edges, MeshType.Lines, AccessType.Dynamic);
        }

        public override void OnUpdate()
        {
            if (InputManager.IsKeyDown(Settings.MoveFrameKeybinding))
            {
                var currentRayLine = Scene.SceneCamera.CalculateCameraScreenRayLine(InputManager.MousePosition, Settings.DisplayWidth, Settings.DisplayHeigth);
                var mousePlaneDir = _movementPlane.CalculateRayIntersetion(currentRayLine) - _movementIntersect;
                Position = _initialPosition + mousePlaneDir;
            }

            if (InputManager.IsKeyDown(Settings.RotateFrameKeybinding))
            {
                var currentRayLine = Scene.SceneCamera.CalculateCameraScreenRayLine(InputManager.MousePosition, Settings.DisplayWidth, Settings.DisplayHeigth);
                var mousePlaneDir = _movementPlane.CalculateRayIntersetion(currentRayLine) - _movementIntersect;
                Rotation = Scene.SceneCamera.Rotation * Quaternion.FromAxisAngle(Vector3.UnitX, -mousePlaneDir.Y) * Quaternion.FromAxisAngle(Vector3.UnitY, mousePlaneDir.X + -mousePlaneDir.Z);
            }

            UpdateFramePointsPositions();
        }

        private void UpdateFramePointsPositions()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        var rotPos = Rotation.Inverted() * Position;
                        FramePoints[k, i, j].Model.Position = Rotation * new Vector3(rotPos.X + FrameSide * k + offsetX,
                                                                  rotPos.Y + FrameSide * i + offsetY, rotPos.Z + FrameSide * j + offsetZ);
                    }
                }
            }
        }


        public override void OnRender()
        {
            if (IsVisible)
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                Mesh.Draw();
            }
        }

        public Spring AddSping(JelloPoint one, JelloPoint two)
        {
            var s = new Spring(one, two);
            return s;
        }

        public void Move(Vector2 mousePosition, float windowWidth, float windowHeight)
        {
            if (InputManager.IsMouseButtonDown(MouseButton.XButton2))
            {
                if (_lastMousePosition == mousePosition)
                    return;

                _lastMousePosition = mousePosition;

                var mouseNormX = 2 * mousePosition.X / windowWidth - 1;
                var mouseNormY = -2 * mousePosition.Y / windowHeight - 1;

                //var matrix = (Scene.SceneCamera.GetViewMatrix()).Inverted();
                var matrix = Scene.SceneCamera.GetProjectionMatrix().Inverted() * Scene.SceneCamera.GetViewMatrix().Inverted();

                var v4 = new Vector4(mouseNormX, mouseNormY, 0, 1) * matrix;

                Position = new Vector3(v4.X / v4.W, v4.Y / v4.W, Position.Z);
            }
        }
    }
}

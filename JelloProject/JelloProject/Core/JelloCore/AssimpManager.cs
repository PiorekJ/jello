﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Assimp;
using JelloProject.OpenTK;
using JelloProject.Utils;
using OpenTK;

namespace JelloProject.Core.JelloCore
{
    public class AssimpManager
    {
        public static Mesh<VertexPNC> LoadMesh(string filepath)
        {
            AssimpContext importer = new AssimpContext();
            var model = importer.ImportFile(filepath, PostProcessSteps.Triangulate);

            if(model == null || model.SceneFlags == SceneFlags.Incomplete || model.RootNode == null)
                throw new Exception("Failed to load");

            List<VertexPNC> vertices = new List<VertexPNC>();
            List<uint> edges = new List<uint>();
            List<Vector3> normals = new List<Vector3>();
            foreach (Mesh mesh in model.Meshes)
            {
                foreach (Vector3D normal in mesh.Normals)
                {
                    normals.Add(normal.ToTKVector3());
                }
                for (var i = 0; i < mesh.Vertices.Count; i++)
                {
                    Vector3D vertex = mesh.Vertices[i];
                    vertices.Add(new VertexPNC(vertex.ToTKVector3(), normals[i], Colors.LawnGreen.ColorToVector3()));
                }
                foreach (Face face in mesh.Faces)
                {
                    foreach (int faceIndex in face.Indices)
                    {
                        edges.Add((uint)faceIndex);
                    }
                }
            }
            return new Mesh<VertexPNC>(vertices,edges,MeshType.Triangles,AccessType.Dynamic);
        }
    }
}

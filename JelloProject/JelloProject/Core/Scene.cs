﻿using System.Collections.Generic;
using System.Drawing;
using JelloProject.Core.JelloCore;
using JelloProject.Models;
using JelloProject.Utils;
using OpenTK;
using Color = System.Windows.Media.Color;

namespace JelloProject.Core
{
    public class Scene: BindableObject
    {
        public Camera SceneCamera;
        public List<Model> SceneModels;
        public SceneLight SceneLight;

        public SceneCollisionBox CollisionBox
        {
            get => _collisionBox;
            set
            {
                _collisionBox = value;
                RaisePropertyChanged();
            }
        }

        public SceneJello SceneJello
        {
            get => _sceneJello;
            set
            {
                _sceneJello = value; 
                RaisePropertyChanged();
            }
        }

        public SceneJelloFrame SceneJelloFrame
        {
            get => _sceneJelloFrame;
            set
            {
                _sceneJelloFrame = value; 
                RaisePropertyChanged();
            }
        }

        private SceneJello _sceneJello;
        private SceneCollisionBox _collisionBox;
        private SceneJelloFrame _sceneJelloFrame;

        public Scene()
        {
            SceneCamera = new Camera(this);
            SceneModels = new List<Model>();
        }

        public ScenePoint AddPoint()
        {
            ScenePoint point = new ScenePoint(this);
            SceneModels.Add(point);
            return point;
        }

        public ScenePoint AddPoint(Vector3 position)
        {
            ScenePoint point = new ScenePoint(this);
            point.Position = position;
            SceneModels.Add(point);
            return point;
        }

        public ScenePoint AddPoint(ScenePoint point)
        {
            SceneModels.Add(point);
            return point;
        }

        public SceneLight AddSceneLight()
        {
            SceneLight = new SceneLight(this);
            SceneModels.Add(SceneLight);
            return SceneLight;
        }

        public SceneCursor AddSceneCursor()
        {
            SceneCursor test = new SceneCursor(this);
            SceneModels.Add(test);
            return test;
        }

        public FloorGridModel AddFloorGrid()
        {
            FloorGridModel floorGridModel = new FloorGridModel(this);
            SceneModels.Add(floorGridModel);
            return floorGridModel;
        }

        public SceneJello AddJello()
        {
            SceneJello = new SceneJello(this);
            SceneModels.Add(SceneJello);
            return SceneJello;
        }

        public SceneJelloFrame AddJelloFrame()
        {
            SceneJelloFrame = new SceneJelloFrame(this);
            SceneModels.Add(SceneJelloFrame);
            return SceneJelloFrame;
        }

        public SceneTestModel AddSceneTestModel()
        {
            SceneTestModel test = new SceneTestModel(this);
            SceneModels.Add(test);
            return test;
        }

        public SceneCollisionBox AddCollisionBoxModel()
        {
            CollisionBox = new SceneCollisionBox(this);
            SceneModels.Add(CollisionBox);
            return CollisionBox;
        }
    }
}
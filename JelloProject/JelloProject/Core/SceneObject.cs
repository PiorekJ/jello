﻿using JelloProject.Utils;
using JelloProject.Utils.LogicModels;
using OpenTK;

namespace JelloProject.Core
{
    public interface ISelectable
    {
        void Select();
        void Deselect();
    }

    public interface ITransform
    {
        void Translate(Vector3 newPosition);
        void Rotate(Vector3 newRotation);
        void Scale(Vector3 newScale);
    }

    public abstract class SceneObject : BindableObject
    {
        public Vector3 Position { get; set; } = Vector3.Zero;
        public Quaternion Rotation { get; set; } = Quaternion.Identity;
        public Vector3 Scale { get; set; } = Vector3.One;

        public string ObjectName { get; set; }

        public Scene Scene;

        protected SceneObject(Scene currScene)
        {
            Scene = currScene;
        }

        public Matrix4 GetModelMatrix()
        {
            return Matrix4.CreateScale(Scale) * Matrix4.CreateFromQuaternion(Rotation) * Matrix4.CreateTranslation(Position);
        }

        public virtual void OnUpdate() { }
        public virtual void OnRender() { }
    }
}

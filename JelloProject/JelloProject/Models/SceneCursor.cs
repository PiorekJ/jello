﻿using System.Collections.Generic;
using JelloProject.Core;
using JelloProject.OpenTK;
using OpenTK;

namespace JelloProject.Models
{
    public class SceneCursor : Model
    {
        public SceneCursor(Scene currScene) : base(currScene)
        {
            Position = Vector3.Zero;
            Shader = Shaders.BasicShader;
            Mesh = GenerateCursorMesh();
        }

        public override void SetMesh()
        {
            Mesh = GenerateCursorMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        public override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Mesh.Draw();
        }

        private Mesh<VertexPC> GenerateCursorMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.UnitX * 0.5f, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.UnitY * 0.5f, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.Zero, Vector3.UnitZ));
            vertices.Add(new VertexPC(Vector3.UnitZ * 0.5f, Vector3.UnitZ));

            return new Mesh<VertexPC>(vertices, null, MeshType.Lines, AccessType.Static);
        }
    }
}

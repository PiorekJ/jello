﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 color;

out VS_OUT 
{
	vec3 position;
	vec3 color;
} Out;

uniform mat4 view;
uniform mat4 projection;

mat4 projectionView = projection * view;

void main()
{
	gl_Position = projectionView * vec4(position,1);
	Out.color = color;
	Out.position = position;
}

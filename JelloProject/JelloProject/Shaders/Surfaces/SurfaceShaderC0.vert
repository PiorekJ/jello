﻿#version 330 core

in layout(location = 0) vec2 positionUV;

void main()
{
	gl_Position = vec4(positionUV,0,0);
}
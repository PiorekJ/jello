﻿using JelloProject.OpenTK;

namespace JelloProject.Core
{
    public static class Shaders
    {
        private static Shader _basicShader;
        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _textureShader;
        public static Shader TextureShader
        {
            get
            {
                if (_textureShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Texture/TextureShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Texture/TextureShader.frag"))
                    {
                        _textureShader = new Shader(vert, frag);
                    }
                    return _textureShader;
                }
                return _textureShader;
            }
        }

        private static Shader _lightShader;
        public static Shader LightShader
        {
            get
            {
                if (_lightShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Light/LightShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Light/LightShader.frag"))
                    {
                        _lightShader = new Shader(vert, frag);
                    }
                    return _lightShader;
                }
                return _lightShader;
            }
        }

        private static Shader _litObjectShader;
        public static Shader LitObjectShader
        {
            get
            {
                if (_litObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Object/LightObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Object/LightObjectShader.frag"))
                    {
                        _litObjectShader = new Shader(vert, frag);
                    }
                    return _litObjectShader;
                }
                return _litObjectShader;
            }
        }

        private static Shader _bezierCubeShader;
        public static Shader BezierCubeShader
        {
            get
            {
                if (_bezierCubeShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BezierCube/BezierCubeshader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BezierCube/BezierCubeshader.frag")/*,
                        geom = new GeometricShaderObject("./Shaders/BezierCube/BezierCubeshader.geom")*/)
                    {
                        _bezierCubeShader = new Shader(vert, frag/*, geom*/);
                    }
                    return _bezierCubeShader;
                }
                return _bezierCubeShader;
            }
        }

        private static Shader _bezierCubeShaderNonLit;
        public static Shader BezierCubeShaderNonLit
        {
            get
            {
                if (_bezierCubeShaderNonLit == null)
                {
                    using (ShaderObject vert =
                            new VertexShaderObject("./Shaders/BezierCubeNonLit/BezierCubeShaderNonLit.vert"),
                        frag = new FragmentShaderObject("./Shaders/BezierCubeNonLit/BezierCubeShaderNonLit.frag"))

                    {
                        _bezierCubeShaderNonLit = new Shader(vert, frag);
                    }
                    return _bezierCubeShaderNonLit;
                }
                return _bezierCubeShaderNonLit;
            }
        }
    }
}

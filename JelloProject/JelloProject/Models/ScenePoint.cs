﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using JelloProject.Core;
using JelloProject.OpenTK;
using JelloProject.Utils;
using OpenTK;

namespace JelloProject.Models
{
    public class ScenePoint : Model, IColorable
    {
        private Color _color = Colors.Red;
        private bool _isVisible = true;

        public Color Color
        {
            get => _color;
            set
            {
                _color = value;
                SetMesh();
                RaisePropertyChanged();
            }
        }

        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                _isVisible = value;
                RaisePropertyChanged();
            }
        }

        public ScenePoint(Scene currScene) : base(currScene)
        {
            Shader = Shaders.BasicShader;
            Mesh = GeneratePointMesh();
        }

        public override void SetMesh()
        {
            Mesh = GeneratePointMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        private IMesh GeneratePointMesh()
        {
            Mesh?.Dispose();
            return new Mesh<VertexPC>(new List<VertexPC>() { new VertexPC(Position, Color.ColorToVector3()) }, null, MeshType.Points, AccessType.Static);
        }

        public override void OnUpdate()
        {
        }

        public override void OnRender()
        {
            if (IsVisible)
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                Mesh.Draw();
            }
        }
    }
}

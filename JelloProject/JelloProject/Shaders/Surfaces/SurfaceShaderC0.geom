﻿#version 330 core

layout (lines) in;
layout (line_strip, max_vertices = 8) out; //points


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 points[16];  
uniform bool drawVectors;

mat4 projectionViewModel = projection * view * model;

out GEO_OUT 
{
	vec3 color;
} Out;

vec3 deCasteljau(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
	float paramT = (1-t);
	
	a = a * paramT + b*t;
	b = b * paramT + c*t;
	c = c * paramT + d*t;

	a = a * paramT + b*t;
	b = b * paramT + c*t;
	
	a = a * paramT + b*t;

	return a;
}

vec3 derivative(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
	float paramT = 1 - t;

    vec3 deriv0 = 3 * (b - a);
    vec3 deriv1 = 3 * (c - b);
    vec3 deriv2 = 3 * (d - c);

    vec3 r00 = deriv0 * paramT + deriv1 * t;
    vec3 r10 = deriv1 * paramT + deriv2 * t;

    return r00 * paramT + r10 * t;
}

vec3 surfaceCalc(vec2 positionUV)
{
	vec3 sumLOne = deCasteljau(positionUV.y, points[0], points[4], points[8], points[12]);
	vec3 sumLTwo = deCasteljau(positionUV.y, points[1], points[5], points[9], points[13]);
	vec3 sumLThree = deCasteljau(positionUV.y, points[2], points[6], points[10], points[14]);
	vec3 sumLFour = deCasteljau(positionUV.y, points[3], points[7], points[11], points[15]);

	vec3 sumR = deCasteljau(positionUV.x, sumLOne, sumLTwo, sumLThree, sumLFour);
	
	return sumR;
}

vec3 uDerivative(float u, float v)
{
	vec3 a = deCasteljau(v, points[0], points[4], points[8], points[12]);
	vec3 b = deCasteljau(v, points[1], points[5], points[9], points[13]);
	vec3 c = deCasteljau(v, points[2], points[6], points[10], points[14]);
	vec3 d = deCasteljau(v, points[3], points[7], points[11], points[15]);
	
	vec3 ret = derivative(u, a, b, c, d);

	return ret;
}

vec3 vDerivative(float u, float v)
{
	vec3 a = deCasteljau(u, points[0], points[1], points[2], points[3]);
	vec3 b = deCasteljau(u, points[4], points[5], points[6], points[7]);
	vec3 c = deCasteljau(u, points[8], points[9], points[10], points[11]);
	vec3 d = deCasteljau(u, points[12], points[13], points[14], points[15]);

	vec3 ret = derivative(v, a, b, c, d);

	return ret;
}

void DrawPrimitive(vec4 start, vec4 end, vec3 color)
{
	Out.color = color;
	gl_Position = start;
	EmitVertex();
	gl_Position = end;
	EmitVertex();
	EndPrimitive();
}

void main()
{	
	vec2 startUV = vec2(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y);
	vec2 endUV = vec2(gl_in[1].gl_Position.x, gl_in[1].gl_Position.y);
	
	vec3 lineStart = surfaceCalc(startUV);
	vec3 lineEnd = surfaceCalc(endUV);
	vec4 sceneLineStart = projectionViewModel * vec4(lineStart, 1.0);
	vec4 sceneLineEnd = projectionViewModel * vec4(lineEnd, 1.0);
	DrawPrimitive(sceneLineStart, sceneLineEnd, vec3(1,1,1));

	if(drawVectors)
	{
		vec3 uDeriv = uDerivative(startUV.x, startUV.y);
		vec3 vDeriv = vDerivative(startUV.x, startUV.y);
		vec3 tangent = cross(uDeriv,vDeriv);
		vec4 sceneDerivU = projectionViewModel * vec4(lineStart + uDeriv * 0.1, 1.0);
		vec4 sceneDerivV = projectionViewModel * vec4(lineStart + vDeriv * 0.1, 1.0);
		vec4 sceneTangent = projectionViewModel * vec4(lineStart + tangent * 0.1, 1.0);
		DrawPrimitive(sceneLineStart, sceneDerivU, vec3(1,0,0)); // RED - U Derivative
		DrawPrimitive(sceneLineStart, sceneDerivV, vec3(0,1,0)); // GREEN - V Derivative
		DrawPrimitive(sceneLineStart, sceneTangent, vec3(0,0,1)); // BLUE - Tangent
	}
	

	
	
} 
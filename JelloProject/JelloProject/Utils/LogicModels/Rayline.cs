﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace JelloProject.Utils
{
    public class Rayline
    {
        public Vector3 Origin;
        public Vector3 Direction;

        public Rayline(Vector3 origin, Vector3 direction)
        {
            Origin = origin;
            Direction = direction;
        }
    }
}

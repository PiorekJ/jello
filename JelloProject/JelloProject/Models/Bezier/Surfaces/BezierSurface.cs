﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Windows.Input;
//using JelloProject.Core;
//using JelloProject.OpenTK;
//using OpenTK;

//namespace JelloProject.Models.Bezier.Surfaces
//{
//    public abstract class BezierSurface : BaseModel, IIntersectable
//    {
//        private int _uParameter = Constants.BaseSurfaceUParamter;
//        private int _vParameter = Constants.BaseSurfaceVParamter;
//        private bool _displayBernsteinPolynomial;

//        protected VertexPC[] VerticesPoly { get; set; }
//        protected Shader PolyShader { get; set; }
//        protected Mesh<VertexPC> PolyLines { get; set; }

//        public int PatchesX;
//        public int PatchesY;

//        public bool IsUWrapped = false;
//        public bool IsVWrapped = false;

//        public int UParameter
//        {
//            get { return _uParameter; }
//            set
//            {
//                _uParameter = value;
//                SetMesh();
//            }
//        }

//        public int VParameter
//        {
//            get { return _vParameter; }
//            set
//            {
//                _vParameter = value;
//                SetMesh();
//            }
//        }

//        public ObservableCollection<PointModel> SurfacePoints { get; set; }
//        public List<BezierSurfacePatch> SurfacePatches;

//        public override void Initialize(Simulation simulation)
//        {
//            SurfacePoints = new ObservableCollection<PointModel>();
//            SurfacePatches = new List<BezierSurfacePatch>();
//            base.Initialize(simulation);
//        }

//        public void InitializeSurface(int type, float sizeX, float sizeY, int patchesCountX, int patchesCountY)
//        {
//            PatchesX = patchesCountX;
//            PatchesY = patchesCountY;
//            switch (type)
//            {
//                case 0:
//                    CreatePlaneSurfacePoints(sizeX, sizeY, patchesCountX, patchesCountY);
//                    CreatePlaneSurfacePatches(patchesCountX, patchesCountY);

//            }
//        }

//        protected abstract void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY);
        
//        protected abstract void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY);
        

//        public override void DeleteObject(IMesh mesh)
//        {
//            foreach (PointModel pointModel in SurfacePoints)
//            {
//                pointModel.DeleteObject(pointModel.Mesh);
//            }
//            base.DeleteObject(mesh);
//        }

//        public override void Dispose()
//        {
//            PolyLines?.Dispose();
//            base.Dispose();
//        }

//        protected override void SetMesh()
//        {
//            Mesh?.Dispose();
//            List<Vertex2DP> vertices = new List<Vertex2DP>();
//            List<uint> edges = new List<uint>();
//            float distX = 1f / (UParameter - 1);
//            float distY = 1f / (VParameter - 1);
//            for (int i = 0; i < VParameter; i++)
//            {
//                for (int j = 0; j < UParameter; j++)
//                {
//                    vertices.Add(new Vertex2DP(j * distX, i * distY));
//                    if (j != 0)
//                    {
//                        edges.Add((uint)(j + i * UParameter));
//                        edges.Add((uint)((j - 1) + i * UParameter));
//                    }
//                    if (i != 0)
//                    {
//                        edges.Add((uint)(j + i * UParameter));
//                        edges.Add((uint)(j + (i - 1) * UParameter));
//                    }
//                }
//            }

//            Mesh = new Mesh<Vertex2DP>(vertices, edges, MeshType.Lines);
//        }

//        public override void Draw()
//        {
//            Shader.Activate();
//            Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
//            Shader.Bind(Shader.GetUniformLocation("view"), SimulationRef.SceneCamera.GetViewMatrix());
//            Shader.Bind(Shader.GetUniformLocation("projection"), SimulationRef.SceneCamera.GetProjectionMatrix());
//            Shader.Bind(Shader.GetUniformLocation("drawVectors"), false);
//            foreach (BezierSurfacePatch surfacePatch in SurfacePatches)
//            {
//                for (int i = 0; i < 16; i++)
//                {
//                    Shader.Bind(Shader.GetUniformLocation($"points[{i}]"), surfacePatch.PatchPoints[i].Position);
//                }
//                Mesh.Draw();
//            }
//        }
        
//    }
//}

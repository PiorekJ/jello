﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace JelloProject.Utils.LogicModels
{
    public class Plane
    {
        public Vector3 Origin;
        public Vector3 Normal;

        public Plane(Vector3 origin, Vector3 normal)
        {
            Origin = origin;
            Normal = normal;
        }

        public Plane()
        {
            
        }

        public Vector3 CalculateRayIntersetion(Rayline rayline)
        {
            float normalDirDot = Vector3.Dot(rayline.Direction, Normal);
            if (normalDirDot < 0.00001f && normalDirDot > -0.00001f)
                return Vector3.Zero; //TODO: CHANGE TO SOMETHING ELSE
            Vector3 paramterC = rayline.Origin - Origin;
            float paramterT = -(Vector3.Dot(paramterC, Normal) / normalDirDot);
            if (paramterT < 0)
                return Vector3.Zero;

            return rayline.Origin + paramterT * rayline.Direction;
        }
    }
}
